
public class HelloDoWhileLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count=10; //1. initilize
		do {
			System.out.println("Hello " + count);
			count++; //3. increment
		} while(count<10); //2. condition
	}

}
