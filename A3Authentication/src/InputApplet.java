import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class InputApplet extends JApplet {

	public InputApplet() {
		// TODO Auto-generated method stub
		JPanel pane = new JPanel();
		JLabel jlblUname = new JLabel("Username");
		JLabel jlblpw= new JLabel("Password");
		JTextField jtxtUserName = new JTextField(10);
		JTextField jtxtPassword = new JTextField(10);
		pane.add(jlblUname);
		pane.add(jtxtUserName);
		pane.add(jlblpw);
		pane.add(jtxtPassword);
		JOptionPane.showMessageDialog(null, pane);
		System.out.println(jtxtUserName.getText());
		System.out.println(jtxtPassword.getText());
	}
	
	public void init() {
		new InputApplet();
	}

}
