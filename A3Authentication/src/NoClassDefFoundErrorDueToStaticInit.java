import java.awt.List;
import java.util.ArrayList;

/**
  * Java program to demonstrate how failure of static initialization subsequently cause
  * java.lang.NoClassDefFoundError in Java.
  * @author Javin Paul
  */
public class NoClassDefFoundErrorDueToStaticInit {

     public static void main(String args[]){
         
         ArrayList<Integer>users = new ArrayList<Integer>(2);
         
         for(int i=0; i<2; i++){
             try{
            	 users.add(i); //will throw NoClassDefFoundError
             }catch(Throwable t){
                 t.printStackTrace();
             }
         }         
     }
}

class User{
     private static String USER_ID = getUserId();
     
     public User(String id){
         this.USER_ID = id;
     }
     private static String getUserId() {
         throw new RuntimeException("UserId Not found");
     }     
}
