import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class LoopConditionsDemo {
	public static void main(String[] args) throws MalformedURLException {
		//String url = "C:\\Users\\Public\\Pictures\\Sample Pictures\\Desert.jpg";
		
		String url = "C:/Users/Public/Pictures/Sample Pictures/Desert.jpg";
		URL url1= new URL("http://fc02.deviantart.net/fs71/f/2010/285/7/1/random_dragon_gif_by_ribozurai-d30makh.gif");
		ImageIcon icon = new ImageIcon(url1, "java icon");
		
		// &&, ||, !
		String [] options = {"faculty", "student", "staff"};
		int x = 10;
		int y = -10;
		if(!(x > 0)||(y<0)) {
			JOptionPane.showInputDialog(null, "message goes here", "Pick one", JOptionPane.INFORMATION_MESSAGE, icon, options, options[2]);
			
			//JOptionPane.showOptionDialog(null, null, "Test", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, icon, options, 2);
		} else {
			System.out.println((int)Math.random());
		}
	}
}
