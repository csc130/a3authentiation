import javax.swing.JOptionPane;

public class HelloForever {

	public static void main(String[] args) {
		int terminate;
		do {
			terminate = JOptionPane.showConfirmDialog(null, "Continue?");
			System.out.println("Hello ");
		}
		while (terminate != JOptionPane.NO_OPTION)
			;
		JOptionPane.showMessageDialog(null, "Good Bye!");
	}

}
