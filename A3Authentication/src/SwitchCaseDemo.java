import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class SwitchCaseDemo {

	public static void main(String[] args) throws MalformedURLException {
		String correctAccountType = "faculty";
		// TODO Auto-generated method stub
		URL url1 = new URL(
				"http://fc02.deviantart.net/fs71/f/2010/285/7/1/random_dragon_gif_by_ribozurai-d30makh.gif");
		ImageIcon icon = new ImageIcon(url1, "java icon");

		String[] options = { "faculty", "student", "staff" };

		String selected;

		do {

			selected = (String) JOptionPane.showInputDialog(null,
					"message goes here", "Pick one",
					JOptionPane.INFORMATION_MESSAGE, icon, options, options[2]);
			// demonstrate how to use switch case instead
	
		} while (!selected.equals(correctAccountType));
		
		switch (selected) {

		case "faculty":
			JOptionPane.showMessageDialog(null, "Welcome faculty!");
			break;
		case "student":
			JOptionPane.showMessageDialog(null, "Welcome student!");
			break;
		case "staff":
			JOptionPane.showMessageDialog(null, "Welcome staff!");
			break;
		}
	}

}

/*
 * if (selected.equals("faculty")) { JOptionPane.showMessageDialog(null,
 * "Welcome faculty!"); } else if (selected.equals("student")) {
 * JOptionPane.showMessageDialog(null, "Welcome student!"); } else if
 * (selected.equals("staff")) { JOptionPane.showMessageDialog(null,
 * "Welcome staff!"); }
 */